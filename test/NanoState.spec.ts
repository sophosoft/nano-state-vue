import { Store } from '@sophosoft/nano-state';
import { NanoState } from '../src';
import Vue from 'vue';
import { expect } from 'chai';

describe('Vue with NanoState', () => {
  it('contains a $state meta property', () => {
    Vue.use(NanoState, { contexts: {} })

    class Test extends Vue {
      private context = this.$state.getContext('foo')
    }

    // let Test: any = Vue
    const vm = new Test({ template: '<div></div>' }).$mount()
    
    expect(vm.$state).to.be.instanceOf(Store)
  })
})
