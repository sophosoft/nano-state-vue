import { Store, ContextMap, Subscription } from '@sophosoft/nano-state'
import Vue from 'vue'

export interface InstallOptions {
    contexts: ContextMap
    subscriptions?: Subscription[]
}

export function NanoState(v: typeof Vue, options: InstallOptions): void {
    const store = new Store(options.contexts, options.subscriptions || []);
    v.prototype.$state = store
}

declare module 'vue/types/vue' {
    interface VueConstructor {
        $state: Store
    }

    interface Vue {
        $state: Store
    }
}
