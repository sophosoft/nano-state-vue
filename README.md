# nano-state-vue

Vue mixin that initializes a nano-state `Store` for Vue Components.

## installation
```sh
npm i @sophosoft/nano-state-vue
# or
yarn add @sophosoft/nano-state-vue
```

## use
It's generally recommended to initialize the state config in a separate file than your `main.ts`.  Something like the following example:
```ts
import { InstallOptions } from '@sophosoft/nano-state-vue'
// other imports...

export const NanoStateOptions: InstallOptions = {
    contexts: {
        'home': new HomeContext(),
        'admin': new AdminContext(),
        // etc...
    },
    subscriptions: [
        new LogSubscription(),
        // etc...
    ]
}
```
Then, just import that file into your `main.ts`.
```ts
import Vue from 'vue'
import App from './App.vue'
import { NanoState } from '@sophosoft/nano-state-vue'
import { NanoStateOptions } from './state'

Vue.use(NanoState, NanoStateOptions)

new Vue({
  render: h => h(App)
}).$mount('#app')
```
### components
After installation, components will have access to the `$state` property, which is an instance of [nano-state](https://gitlab.com/sophosoft/nano-state) `Store`.
```html
<template>
  <div class="about">
    <h1>{{ Message }}</h1>
  </div>
</template>

<script lang="ts">
import { Component, Vue } from 'vue-property-decorator'
import { AboutContext } from '@state/about'

@Component({})
export default class About extends Vue {
    private context: AboutContext = this.$state.getContext<AboutContext>('about')

    get Message(): string {
        return this.context.Message
    }
}
</script>
```
